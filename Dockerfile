FROM node:12-alpine

LABEL maintainer="gladieux.dimitri@gmail.com"
LABEL version="1.0"
LABEL description="Dockerfile for React Project"

RUN yarn global add create-react-app && yarn