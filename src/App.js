import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import React, { Component } from "react";
import PublicPage from "./Components/PublicPage";
import { Redirect } from "react-router-dom";

function GuardRoute() {
  return function ({ component: Component, ...rest }) {
    return <Route {...rest} render={() => <Redirect to="/" />} />;
  };
}

class App extends Component {
  render() {
    const PrivateRoute = GuardRoute();
    return (
      <Router>
        <Switch>
          <Route path="/" component={PublicPage} />
          <PrivateRoute component={null} />
        </Switch>
      </Router>
    );
  }
}

export default App;
