import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Avatar from "@material-ui/core/Avatar";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    "& > *": {
      margin: theme.spacing(1),
    },
  },
  size: {
    width: theme.spacing(20),
    height: theme.spacing(20),
    textAlign: "center",
  },
}));

export default function About(props) {
  const classes = useStyles();
  if (props.data) {
    var name = props.data.name;
    var profilepic = "images/me.jpg";
    var bio = props.data.bio;
    var street = props.data.address.street;
    var city = props.data.address.city;
    var state = props.data.address.state;
    var zip = props.data.address.zip;
    var phone = props.data.phone;
    var email = props.data.email;
    var aboutme = props.data.aboutme;
    var contactdetail = props.data.contactdetail;
    var resumeDownload = props.data.resumedownload;
    var textdownload = props.data.textdownload;
  }

  return (
    <section id="about">
      <div className="row">
        <div id="profile-pic" className="three columns">
          <Avatar
            alt="Dimitri Gladieux Cunha pic"
            src={profilepic}
            className={classes.size}
          />
        </div>
        <div className="nine columns main-col">
          <h2>{aboutme}</h2>

          <p>{bio}</p>
          <div className="row">
            <div className="columns contact-details">
              <h2>{contactdetail}</h2>
              <p className="address">
                <span>{name}</span>
                <br />
                <span>
                  {street}
                  <br />
                  {city} {state}, {zip}
                </span>
                <br />
                <span>{phone}</span>
                <br />
                <span>{email}</span>
              </p>
            </div>
            <div className="columns download">
              <p>
                <a href={resumeDownload} download="GladieuxCunhaCV.pdf" className="button">
                  <i className="fa fa-download"></i>
                  {textdownload}
                </a>
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
