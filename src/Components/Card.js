import React, { Component } from 'react';
import { Markup } from 'interweave';
import Divider from '@material-ui/core/Divider'

function Item( { element } ) {

  return (
    <div>
      <div className="container-info info">
        <div className="five columns">
          <i className="fa fa-calendar" aria-hidden="true"></i>{element.years}
        </div>
        <div className="seven columns">
          <i id="map" className="fa fa-map-marker"></i>{element.location}
        </div>
      </div >
      <p className="subtitle-with-info">{element.subtitle}</p>
    </div>
  );
}

class Subtitle extends Component {

  render() {
    return <div>
      {this.props.element.subtitle ? <p className="subtitle-without-info">{this.props.element.subtitle}</p> : ''}
    </div>
  }
}
class Card extends Component {

  render() {

    if ( this.props.data && this.props.id_href ) {
      var title = this.props.data.title;
      var elements = this.props.data.list.map( function ( element, index ) {
        return <div id="container-row" className="row item" key={element.title}>
          <div id="" className="two columns">
            {element.logo ? <div id="picture-row">
              <img src={element.logo} alt="logo" />
            </div> : ''}
          </div>
          <div id="content-row" className="ten columns">
            <h3>{element.title}</h3>

            {element.location ?
              <Item element={element} /> :
              <Subtitle element={element} />
            }
            {element.description ?
              <div className="description">
                <Markup content={element.description} />
              </div> : ''
            }
            <ol>
              {element.tasks ? element.tasks.map( task => <li key={task}>
                <Markup content={task} />
              </li> ) : ''}
            </ol>
          </div>
          <div className="divider">
            <Divider light />
          </div>

        </div>
      } )
    }

    return (

      <section id={this.props.id_href}>
        <div className="row card">
          <div className="header-col">
            <h1><span>{title}</span></h1>
          </div>
          {elements}
        </div>
      </section>
    );
  }
}

export default Card;
