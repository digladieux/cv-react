import React, { Component } from 'react';

class Contact extends Component {
   render() {

      if ( this.props.data ) {
         var name = this.props.data.name;
         var street = this.props.data.address.street;
         var city = this.props.data.address.city;
         var state = this.props.data.address.state;
         var zip = this.props.data.address.zip;
         var phone = this.props.data.phone;
         var email = this.props.data.email;
         var message = this.props.data.contactmessage;
      }

      return (
         <section id="contact">

            <div className="row section-head">

               <div className="twelve columns">

                  <p className="lead">{message}</p>

               </div>

            </div>

            <div className="row">
               <div className="twelve columns">
                  <div className="widget widget_contact">

                     <h4>{name}</h4>
                     <p className="address">
                        {email}<br />
                        {street} <br />
                        {city}, {state} {zip}<br />
                        <span>{phone}</span>
                     </p>
                  </div>
               </div>
            </div>
         </section>
      );
   }
}

export default Contact;
