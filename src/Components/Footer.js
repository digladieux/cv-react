import React, { Component } from 'react';

class Footer extends Component {
  render() {

    if ( this.props.social && this.props.footer ) {
      var arrow = this.props.footer.arrow;
      var date = this.props.footer.date;
      var networks = this.props.social.map( function ( network ) {
        return <li key={network.name}><a href={network.url}><i className={network.className}></i></a></li>
      } )
    }

    return (
      <footer>

        <div className="row">
          <div className="twelve columns">
            <ul className="social-links">
              {networks}
            </ul>

            <ul className="footer">
              <li>© Copyright 2022 Dimitri Gladieux Cunha </li>
              <li>{date}</li>
              <li>Designed by <a title="Styleshout" href="http://www.styleshout.com/">Styleshout</a></li>
            </ul>

          </div>
          <div id="go-top"><a className="smoothscroll" title={arrow} href="#home"><i className="icon-up-open"></i></a></div>
        </div>
      </footer>
    );
  }
}

export default Footer;
