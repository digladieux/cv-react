import React, { Component } from 'react';
import NavBar from './Navbar';

class Header extends Component {
   render() {

      if ( this.props.data && this.props.social ) {

         var navbar = this.props.data.navbar;
         var headline = this.props.data.headline;
         var description = this.props.data.description;
         var networks = this.props.social.map( function ( network ) {
            return <li key={network.name}><a href={network.url}><i className={network.className}></i></a></li>
         } )
      }

      return (
         <header id="home">

            <NavBar parentCallback={this.props.parentCallback} data={navbar} />
            <div className="row banner">
               <div className="banner-text">
                  <h1 className="responsive-headline">Dimitri Gladieux Cunha</h1>
                  <h1 className="responsive-headline">{headline}</h1>
                  <h3>{description}</h3>
                  <hr />
                  <ul className="social">
                     {networks}

                  </ul>
               </div>
            </div>

         </header>
      );
   }
}

export default Header;
