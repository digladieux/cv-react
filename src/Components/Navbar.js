import "react-flags-select/css/react-flags-select.css";
import React, { Component } from "react";

class NavBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      country: "FR",
    };
  }

  onSelectCountry = (countryCode) => {
    this.setState({ country: countryCode });
    this.props.parentCallback(countryCode);
  };

  render() {
    if (this.props.data) {
      var about = this.props.data.about;
      var home = this.props.data.home;
      var resume = this.props.data.resume;
      var contact = this.props.data.contact;
    }

    return (
      <nav id="nav-wrap">
        <a className="mobile-btn" href="#nav-wrap" title="Show navigation">
          Show navigation
        </a>
        <a className="mobile-btn" href="#home" title="Hide navigation">
          Hide navigation
        </a>

        <ul id="nav" className="nav">
          <li className="current">
            <a className="smoothscroll" href="#home">
              {home}
            </a>
          </li>
          <li>
            <a className="smoothscroll" href="#about">
              {about}
            </a>
          </li>
          <li>
            <a className="smoothscroll" href="#resume">
              {resume}
            </a>
          </li>
          <li>
            <a className="smoothscroll" href="#contact">
              {contact}
            </a>
          </li>
          <li>
            <a
              className="smoothscroll"
              href="#home"
              onClick={() => this.onSelectCountry("FR")}
            >
              FR
            </a>
          </li>
          <li>
            <a
              className="smoothscroll"
              href="#home"
              onClick={() => this.onSelectCountry("GB")}
            >
              GB
            </a>
          </li>
        </ul>
      </nav>
    );
  }
}
export default NavBar;
