import React, { Component } from "react";
import Header from "../Components/Header";
import Footer from "../Components/Footer";
import About from "../Components/About";
import Resume from "../Components/Resume";
import Contact from "../Components/Contact";
import englishResume from "../Data/EnglishResume";
import frenchResume from "../Data/FrenchResume";
class PublicPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      resumeData: frenchResume,
    };
  }

  getResumeData(value) {
    switch (value) {
      case "GB":
        this.setState({ resumeData: englishResume });
        break;
      case "FR":
        this.setState({ resumeData: frenchResume });
        break;
      default:
    }
  }

  render() {
    return (
      <div className="App">
        {}
        <Header
          parentCallback={this.getResumeData.bind(this)}
          data={this.state.resumeData.header}
          social={this.state.resumeData.social}
        />
        <About data={this.state.resumeData.about} />
        <Resume data={this.state.resumeData.resume} />
        <Contact data={this.state.resumeData.contact} />
        <Footer
          social={this.state.resumeData.social}
          footer={this.state.resumeData.footer}
        />
      </div>
    );
  }
}

export default PublicPage;
