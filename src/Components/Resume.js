import React, { Component } from 'react';
import Card from './Card';

class Resume extends Component {
  render() {
    if ( this.props.data ) {
      var education = this.props.data.education;
      var work = this.props.data.work;
      var skills = this.props.data.skills;
      var projects = this.props.data.projects;
      var hobbies = this.props.data.hobbies;
      var languages = this.props.data.languages;
    }

    return (
      <section id="resume">
        <Card id_href="work" data={work} />
        <Card id_href="education" data={education} />
        <Card id_href="skills" data={skills} />
        <Card id_href="projects" data={projects} />
        <Card id_href="languages" data={languages} />
        <Card id_href="hobbies" data={hobbies} />

      </section>
    );
  }
}

export default Resume;
