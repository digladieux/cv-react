const englishResume = {
  social: [
    {
      name: "linkedin",
      url: "https://www.linkedin.com/in/dimitri-gladieux-cunha/",
      className: "fa fa-linkedin",
    },
    {
      name: "whatsapp",
      url: "https://api.whatsapp.com/send?phone=+33637664237",
      className: "fa fa-whatsapp",
    },
  ],
  header: {
    headline: "Resume",
    description: "Project Director",
    navbar: {
      about: "About me",
      home: "Home",
      resume: "My journey",
      contact: "Contact me",
    },
  },
  contact: {
    contactmessage:
      "If you want to know me better, I invite you to contact me by e-mail, phone or LinkedIn",
    name: "Dimitri Gladieux Cunha",
    email: "gladieux.dimitri@gmail.com",
    phone: "+33 6 37 66 42 37",
    address: {
      street: "17T Boulevard Maurice Pourchon",
      city: "63100",
      state: "Clermont-Ferrand",
      zip: "FRANCE",
    },
  },
  about: {
    aboutme: "About me",
    contactdetail: "Contact details",
    textdownload: "Download my resume (.pdf)",
    name: "Dimitri Gladieux Cunha",
    bio:
      "Currently a Project Director, I offer my expertise to guide your projects toward success.",
    email: "gladieux.dimitri@gmail.com",
    phone: "+33 6 37 66 42 37",
    address: {
      street: "17T Boulevard Maurice Pourchon",
      city: "63100",
      state: "Clermont-Ferrand",
      zip: "FRANCE",
    },
    website: "https://dimitri-gladieux-cunha.com",
    resumedownload: "images/files/EnglishCV.pdf",
  },
  resume: {
    education: {
      title: "Education",
      list: [
        {
          title: "Master in IT Engineering",
          subtitle:
            "ISIMA Institut Supérieur d’Informatique, de Modélisation et de leurs Applications",
          years: "September 2017 - September 2020",
          location: "Aubière 63170",
          description:
            "Graduate School of Engineering in Computer Science, majoring in Software Engineering and Information Systems.",
          logo: "images/logo/ISIMA.jpg",
        },
        {
          title: "Master in Management - double degree",
          subtitle:
            "Clermont School of Management - IAE (Institut d'Administration des Entreprises)",
          years: "September 2019 - September 2020",
          location: "Clermont-Ferrand 63170",
          description:
            "Graduate School of Management and Administration of Companies within the French Public Research Universities.",
          logo: "images/logo/IAE.png",
        },
      ],
    },
    work: {
      title: "Experiences",
      list: [
        {
          title: "Monkey Factory | MaaSify & MyBus",
          location: "Clermont-Ferrand (63000) - France",
          description:
            `Monkey Factory develops 2 solutions.
            MyBus is an application available on all platforms. In partnership with many urban transport networks, the application offers bus schedules, route calculation, real-time traffic for public transport, as well as dematerialized transport tickets. 
            Maasify aims to meet the new challenges of mobility (MaaS: Mobility as a Service). It is a solution that we offer to third parties.<br><br>`,
          subtitle: "Project Director (Hybrid)",
          years: "October 2023",
          logo: "images/logo/MonkeyFactory.png",
          tasks: [
            `
            <b>Direction</b>
            <ol>
              <li><b>Support</b> and <b>supervision of teams</b> across all projects and Third-Party Application Maintenance (TMA)</li>
              <li><b>Crisis</b> management on various issues</li>
              <li><b>Prioritization</b> based on different deadlines</li>
              <li>Participation in the development of the <b>company’s strategy</b></li>
            </ol>
            <br>`,
          ],
        },
        {
          location: "Clermont-Ferrand - France",
          subtitle: "Project Manager (Remotely)",
          years: "November 2022 - October 2023",
          logo: "images/logo/MonkeyFactory.png",
          tasks: [
            `
            <b>Tools</b>
            <ol>
              <li>Planning : <b>Jira</b></li>
            </ol>
            <b>Client relations</b>
            <ol>
              <li>Gathering client requirements (workshops, proposals, etc.)</li>
              <li>Organizing and facilitating project committee meetings.</li>
            </ol><br>`,
            `<b>Functional Tasks</b>
            <ol>
              <li>Writing user stories</li>
              <li>Estimating and planning projects</li>
              <li>Strategizing product development and evolution</li>
            </ol>
            <br>`,
          ],
        },
        {
          location: "Clermont-Ferrand (63000) - France",
          subtitle: "Lead Dev Backend (Remotely)",
          years: "July 2021 - February 2023",
          logo: "images/logo/MonkeyFactory.png",
          tasks: [
            `<b>Technologies</b> and <b>keywords</b> :
            <ol>
              <li>Organization : <b>Squad Agile</b>.</li>
              <li>Backlog, Roadmap and Versionning : <b>GitLab</b>.</li>
              <li>Framework NodeJs : <b>NestJs</b>.</li>
              <li>Language : <b>TypeScript</b>.</li>
              <li>Package manager : <b>Yarn</b></li>
              <li>Documentation : <b>Compodoc</b>, <b>Dependency Cruiser</b>.</li>
              <li>Tests : End-To-End and Units with <b>Jest</b>.</li>
              <li>API : <b>GraphQL</b> with Apollo Server, <b>REST</b> with a Swagger.</li>
              <li>Message broker : <b>RabbitMQ</b>.</li>
              <li>Monitoring : <b>Grafana</b>, <b>Sentry</b>, <b>Apollo</b>.</li>
              <li>Database : <b>PostgreSQL</b>, <b>InfluxDb</b>, <b>Redis</b>.</li>
              <li>DevOps : <b>Gitlab-Ci</b>, <b>Docker</b>, <b>Kubernetes</b> (<b>Kustomize</b>, Helm).</li>
              <li>Hosting : <b>AKS</b> (Azure Kubernetes Service).</li>
              <li>E-Commerce : <b>Vendure</b>.</li>
              <li>Authentication and User Access Management : <b>Keycloak</b>, token <b>JWT</b>.</li>
              <li>Travel Information : <b>Navitia</b>, <b>OpenStretMap</b>, <b>GTFS</b>, NTFS, SIRI.</li>
              <li>Blockchain : <b>Solidity</b> et Token ERC20.</li>
              <li>Geocoder : Mimirsbrunn and Azure Maps Geocoding.</li>
            </ol><br>`,
            `<b>Functional Tasks</b>
            <ol>
              <li>Thinking with the architect on our infrastructure</li>
              <li>Support newcomers (alternating students, interns, employees)</li>
              <li>Training on our tools</li>
              <li>Writing tasks and estimates</li>
              <li>Sprint review</li>
            </ol><br>`,
          ],
        },
        {
          logo: "images/logo/MaaSify.png",
          location: "Clermont-Ferrand (63000) - France",
          subtitle: "Backend Developer (Remotely)",
          years: "September 2020 - June 2021",
          tasks: [
            `<b>DevOps</b>
            <ol>
              <li>Managing production releases</li>
              <li>Writing kubernetes deployments with Helm and Kustomize</li>
              <li>Writing CI with Gitlab-CI</li>
              <li>Writing Docker images to launch our applications</li>
            </ol><br>`,
            `<b>User Access Management</b>
            <ol>
              <li>Implementation of all authentication and organization of user accounts with Keycloak</li>
              <li>Administration of roles and groups</li>
              <li>Defining quotas for each third party</li>
              <li>Management of JWT tokens</li>
            </ol><br>`,
            `<b>Monitoring</b>
            <ol>
              <li>Implementation of monitoring tools for our infrastructure</li>
              <li>Sentry: bug reporting</li>
              <li>Apollo Engine : APIs usage and tracking progress</li>
              <li>InfluxDb and Grafana: Real-time feedback of microservices</li>
            </ol><br>`,
            `<b>Creation of connectors and gateways</b>.
            <ol>
              <li>Advance - delay of public transportation</li>
              <li>Dynamic data of points of interest: number of available spaces in a parking lot, number of bikes available for rent, ...</li>
              <li>Real time positions of vehicles (scooters, cabs, etc)</li>
            </ol><br>`,
          ],
        },
        {
          logo: "images/logo/MyBus.jpg",
          location: "Clermont-Ferrand (63000) - France",
          subtitle: "Intern Backend Developer (Hybrid)",
          years: "April 2020 - September 2020",
          tasks: [
            `<b>Static data extraction from different sources</b>
            <ol>
              <li>GTFS - Realtime data for public transport</li>
              <li>GBFS - Realtime data for urban bikes</li>
              <li>Open Street Map - World map data</li>
              <li>Feedback of this information in Navitia</li>
            </ol><br>
            `,
            `<b>Blockchain : Smart Contract with Ethereum</b>
            <ol>
              <li>Development of private blockchain</li>
              <li>Creation of Smart Contract using Solidity language</li>
              <li>Initialization of the MyBus token</li>
            </ol><br>`,
            `<b>Side tasks</b>
            <ol>
              <li>Synchronization of models and database using Typeorm migration</li>
              <li>Development of Virtual Payment Terminal with Dalenys platform</li>
              <li>Interactive online documentation using Compodoc and Depencendy Cruiser</li>
              <li>Moving from microservices architecture to monorepo</li>
            </ol><br>`,
          ],
        },
        {
          title: "OrcaWise",
          location: "Dublin - Ireland",
          subtitle: "Full Stack Developer Intern",
          years: "April 2019 - September 2019",
          description:
            "Development of a new kind of CRM. The solution monitors organizations to provide companies with a list of future potential clients, to craft tailored outbound sales messages, and to get leads through multiple communication channels. Leads are scored and pushed to CRM",
          tasks: [
            `<b>Development of an API REST</b>
            <ol>
              <li>.Net Core, C#</li>
              <li>Interaction with a Database in MongoDB (NoSql)</li>
              <li>Secure the API, to be used only by our customer</li>
              <li>Use of Nginx to hide the URL of the API</li>
              <li>Token JWT to establish the connection between the user and the API</li>
              <li>Encryption of all sensible data</li>
              <li>Deployed inside the server of the company</li>
              <li>Unit Test with Mockup of the Database</li>
              <li>Documentation</li>
            </ol><br>`,
            `<b>Development of a web application</b>
            <ol>
              <li>Angular 8, HTML, SCSS, TypeScript</li>
              <li>Secure the access of the dashboard</li>
              <li>Deployed inside the server of the company</li>
              <li>Audit on my own website using WebPageTest and ThinkWithGoogle</li>
            </ol><br>`,
            `<b>NLP: Analyse sentences from RSS feed</b>
            <ol>
              <li>Python</li>
              <li>Spacy library to apply NLP</li>
              <li>Extraction of keywords from newspapers</li>
            </ol><br>`,
            `<b>Find open-source tools instead of using paid applications</b>
            <ol>
              <li>Windows Server to Linux Server</li>
              <li>Private Repository from Github to Gitlab, in order to use Gitlab-Ci</li>
              <li>.NET Framework to .NET Core because free and cross-platform</li>
              <li>Scheduled Task: Jira to Trello to manage the interns</li>
              <li>Communication: Google Hangout to Slack</li>
            </ol><br>`,
            `<b>Management of 12 trainees in order to continue and improve my projects</b>
            <ol>
              <li>Organize the training</li>
              <li>Write the UserStory for three groups of four interns</li>
              <li>Documentation of the task and the technology they need</li>
              <li>Estimate the time they need </li>
              <li>Give tips and help if they have problems</li>
            </ol><br>
            `,
          ],
          logo: "images/logo/orcawise.png",
        },
        {
          title: "Lidl",
          location: "Vichy 03200 - France",
          subtitle: "Cashier and Self-Service Assistant in LIDL",
          years: "July 2015 - March 2019",
          description: "Student job : week-ends and holidays",
          tasks: [
            "Handling the cash register according to company <b>procedure</b> and <b>advising</b> clients",
            "Shelving all types of products and maintaining the shelf displays",
            "<b>Inventory management</b>",
            "<b>Training course</b>: HACCP, respect of the cold chain, electric pallet truck driving, cardboard baler and scrubber-dryer",
            "<b>Work in team</b> and <b>participate in meetings</b>",
          ],
          logo: "images/logo/Lidl.png",
        },
      ],
    },
    projects: {
      title: "Academic Projects",
      list: [
        {
          title: "TrackTrip",
          subtitle: "100 hours",
          years: "2020",
          location: "3rd year at ISIMA",
          description:
            "Development of a social network similar to Instagram, desktop and mobile",
          tasks: [
            "<b>FrontEnd</b> : React (TypeScript) ",
            "<b>BackEnd </b> : API en NodeJs Express",
            "<b>Host FrontEnd</b> : GitLab pages",
            "<b>Host BackEnd</b> : Google Cloud Platform",
            "<b>Continuous Integration</b> : GitLab-CI",
          ],
          logo: "images/icons/tracktrip.png",
        },
        {
          title: "Sumo Robot",
          subtitle: "50 hours",
          years: "2017 - 2018",
          location: "First year at ISIMA",
          description:
            "Robotics project whose purpose was to fight another robot in an arena by pushing it outside.",
          tasks: [
            "Building a robot with a lot of constraints, given by a <b>design brief</b>.",
            "Coding in <b>C++</b> and the <b>Mbed </b> OS",
            "Participation in a <b>National Robotic Tournament</b> (Nimes - France, May 2018)",
          ],
          logo: "images/projects/robot.PNG",
        },
        {
          title: "Remote-car controlled by a smartphone",
          subtitle: "80 hours",
          years: "2014 - 2015",
          location: "Last Year of Baccalaureate",
          description:
            "Deleting the radio-controlled system of an R/C car and replacing it with a Bluetooth connection between the smartphone and the vehicle.",
          tasks: [
            "Mobile Application with <b>App Inventor 2</b> (movement of the vehicle with buttons or smartphone's gyroscope)",
            "Microcontroller coding in <b>Arduino</b>",
            "First in the <b>Regional Olympiad of Engineering Science</b> (Clermont-Ferrand - France, April 2015), then <b>National Competition</b> to present it in front of industry professionals. (Paris - France, May 2015)",
          ],
          logo: "images/icons/car.png",
        },
      ],
    },
    skills: {
      title: "Computer skills",
      list: [
        {
          title: "Objects Languages",
          tasks: [
            "<b>Mastered : </b>NestJs (TypeScript), C# (.NET Core & Framework), C++, Java",
            "<b>Knowledged : </b> Rust, Python, Solidity",
            "<b>Used : </b> Swift",
          ],
          logo: "images/logo/object.png",
        },
        {
          title: "Database",
          tasks: [
            "<b>Mastered : </b>SQL (PostgreSQL), NoSQL (with MongoDB), InfluxDb, Redis",
            "<b>Knowledged : </b> MySQL",
          ],
          logo: "images/logo/mongodb.png",
        },
        {
          title: "CI / CD",
          tasks: [
            "<b>Mastered : </b>Gitlab CI, Docker, Kubernetes (Helm and Kustomize), AKS",
            "<b>Knowledged : </b> Google Cloud Platform, VMware vSphere",
          ],
          logo: "images/logo/integration.png",
        },
        {
          title: "Documentation and Unit Testing",
          tasks: [
            "<b>Documentation : </b>Compodoc, Dependency Cruiser, Doxygen, JavaDoc",
            "<b>Unit Testing : </b>Jest (JavaScript), JUnit (Java), WireMock (.NET)",
          ],
          logo: "images/logo/doctest.jpg",
        },
        {
          title: "Tools",
          tasks: [
            "<b>Mastered : </b>RabbitMQ, Grafana, LaTeX, Google Tools, Prezi",
          ],
          logo: "images/logo/tools.png",
        },
        {
          title: "Web Development",
          tasks: [
            "<b>Mastered : </b>Angular 2+ (TypeScript), React (JavaScript), Material UI, Bootstrap",
            "<b>Knowledged : </b>CSS",
          ],
          logo: "images/logo/web.png",
        },
        {
          title: "Operating Systems",
          tasks: [
            "<b>Mastered : </b>Linux, Windows, Android, Mbed",
            "<b>Used : </b> iOS",
          ],
          logo: "images/logo/os.png",
        },
        {
          title: "Other languages",
          tasks: [
            "<b>Mastered : </b>Bash, C, Arduino",
            "<b>Knowledged : </b>Script Linux, Assembly, Mapple",
            "<b>Used : </b> Processing, R, Scheme, Caml",
          ],
          logo: "images/logo/others.png",
        },
      ],
    },
    languages: {
      title: "Languages",
      list: [
        {
          title: "French",
          subtitle: "Native Speaker",
          logo: "images/flag/fr.png",
        },
        {
          title: "English",
          years: "7th march 2019",
          location: "63000 Clermont-Ferrand, France",
          subtitle: "TOEIC : 940/990",
          description:
            "Level before my 2 internships abroad. My english is now <b>fluent</b>",
          logo: "images/flag/gb.png",
        },
        {
          title: "Spanish",
          subtitle: "Intermediate",
          logo: "images/flag/es.png",
        },
      ],
    },
    hobbies: {
      title: "Hobbies",
      list: [
        {
          title: "Black belt in Judo-Jiu-Jitsu",
          location: "63122, Ceyrat- France",
          subtitle: "1st dan",
          years: "November 2014",
          logo: "images/icons/judo.png",
        },
        {
          title: "Military Preparation",
          location: "Haguenau, Camp d'Oberhoffen, France",
          subtitle: "Inside the 28th geographical group of the land forces",
          years: "July 2014",
          logo: "images/logo/army.png",
        },
        {
          title: "Humanitarian Mission",
          location: "Battambang, Cambodia",
          subtitle: "Organization SOURIRES",
          years: "November 2015",
          logo: "images/logo/sourires.png",
          description:
            "Build a Care Center for orphan teenagers from scratch. We worked closely with the local population and the children to construct this place.",
        },
        {
          title: "Trips",
          subtitle:
            "Ireland, Cambodia, Spain, Portugal, Denmark, Italy, United Kingdom, Croatia, Austria, Germany, China",
          logo: "images/icons/trip.jpg",
        },
        {
          title: "Interest",
          subtitle: "Crypto-currency, Blockchain, Innovation",
          logo: "images/icons/bitcoin.png",
        },
      ],
    },
  },
  footer: {
    arrow: "Go back to home",
    date: "April 2022",
  },
};

export default englishResume;
