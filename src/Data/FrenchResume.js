const frenchResume = {
  social: [
    {
      name: "linkedin",
      url: "https://www.linkedin.com/in/dimitri-gladieux-cunha/",
      className: "fa fa-linkedin",
    },
    {
      name: "whatsapp",
      url: "https://api.whatsapp.com/send?phone=+33637664237",
      className: "fa fa-whatsapp",
    },
  ],
  header: {
    headline: "CV",
    description: "Directeur de Projets",
    navbar: {
      about: "A Propos de moi",
      home: "Accueil",
      resume: "Mon parcours",
      contact: "Me Contacter",
    },
  },
  contact: {
    contactmessage:
      "Si vous souhaitez mieux me connaître, je vous invite à me contacter par e-mail, téléphone ou LinkedIn",
    name: "Dimitri Gladieux Cunha",
    email: "gladieux.dimitri@gmail.com",
    phone: "+33 6 37 66 42 37",
    address: {
      street: "17T Boulevard Maurice Pourchon",
      city: "63100",
      state: "Clermont-Ferrand",
      zip: "FRANCE",
    },
  },
  about: {
    aboutme: "A propos de moi",
    contactdetail: "Mes coordonnées",
    textdownload: "Télécharger mon CV (.pdf)",
    name: "Dimitri Gladieux Cunha",
    bio: `Actuellement directeur de projets, je mets mon expertise à votre service pour piloter vos projets vers le succès`,
    email: "gladieux.dimitri@gmail.com",
    phone: "+33 6 37 66 42 37",
    address: {
      street: "17T Boulevard Maurice Pourchon",
      city: "63100",
      state: "Clermont-Ferrand",
      zip: "FRANCE",
    },
    website: "https://dimitri-gladieux-cunha.com",
    resumedownload: "images/files/FrenchCV.pdf",
  },
  resume: {
    education: {
      title: "Education",
      list: [
        {
          title: "Ingénieur en Informatique et Modélisation",
          subtitle:
            "ISIMA Institut Supérieur d’Informatique, de Modélisation et de leurs Applications",
          years: "Septembre 2017 - Septembre 2020",
          location: "Aubière 63170",
          description: "Spécialité Génie Logiciel et Systèmes d'Informations",
          logo: "images/logo/ISIMA.jpg",
        },
        {
          title: "Master MAE - Management et Administration des Entreprises",
          subtitle: "IAE (Institut d'Administration des Entreprises)",
          years: "Septembre 2019 - Septembre 2020",
          location: "Clermont-Ferrand 63170",
          description: "Double diplôme en management",
          logo: "images/logo/IAE.png",
        },
      ],
    },
    work: {
      title: "Experiences",
      list: [
        {
          title: "Monkey Factory | MaaSify & MyBus",
          location: "Clermont-Ferrand (63000) - France",
          subtitle: "Directeur de projets (Hybride)",
          years: "Octobre 2023",
          description:
            `Monkey Factory développe des solutions digitales pour les mobilités du quotidien. A l'aide d'une seule application, d'un seul compte, il est possible de voyager en vélo, en transport en commun, en auto-partage, et tous autre mobilités du quotidien.
            On pourra retrouver au sein de ces applications, les passages aux arrêts, un calculateur d'itinéraire, le traffic en temps réel, la vente et la dématérialisation de titres de transport.<br><br>`,
          logo: "images/logo/MonkeyFactory.png",
          tasks: [
            `
            <b>Direction</b>
            <ol>
              <li><b>Accompagnement</b> et <b>suivi des équipes</b> de l’ensemble des projets et de la Tierce Maintenance Applicative (TMA)</li>
              <li>Gestion des <b>crises</b> sur les différentes problématiques</li>
              <li><b>Priorisation</b> en fonction des différentes échéances</li>
              <li>Participation à l’élaboration de la <b>stratégie d’entreprise</b></li>
            </ol>
            <br>`,
          ],
        },
        {
          location: "Clermont-Ferrand (63000)- France",
          subtitle: "Chef de projet (Distanciel)",
          years: "Novembre 2022 - Octobre 2023",
          logo: "images/logo/MonkeyFactory.png",
          tasks: [
            `
            <b>Outils</b>
            <ol>
              <li>Planification : <b>Jira</b></li>
            </ol>
            <b>Relation Client</b>
            <ol>
              <li>Reccueil du besoin client (Workshop, Propale, etc)</li>
              <li>Organisation et animation des comités projets.</li>
            </ol><br>`,
            `<b>Tâches Fonctionnelles</b>
            <ol>
              <li>Rédaction des users story</li>
              <li>Estimation et planification des projets</li>
              <li>Réflexion sur l'évolution du produit</li>
            </ol>
            <br>`,
          ],
        },
        {
          location: "Clermont-Ferrand (63000)- France",
          subtitle: "Lead Développeur Backend (Distanciel)",
          years: "Juillet 2021 - Février 2023",
          logo: "images/logo/MonkeyFactory.png",
          tasks: [
            `<b>Technologies et mots clés</b>
            <ol>
            <li>Organisation : <b>Squad Agile</b>.</li>
              <li>Backlog, Roadmap et gestionnaire de version : <b>GitLab</b>.</li>
              <li>Framework NodeJs : <b>NestJs</b>.</li>
              <li>Langage : <b>TypeScript</b>.</li>
              <li>Gestionnaire de paquets : <b>Yarn</b>.</li>
              <li>Documentation : <b>Compodoc</b>, <b>Dependency Cruiser</b>.</li>
              <li>Tests : End-To-End et Unitaires avec <b>Jest</b>.</li>
              <li>API : <b>GraphQL</b> avec Apollo Server, <b>REST</b> avec un Swagger.</li>
              <li>Echangeur de messages : <b>RabbitMQ</b>.</li>
              <li>Monitoring : <b>Grafana</b>, <b>Sentry</b>, <b>Apollo</b>.</li>
              <li>Base de données : <b>PostgreSQL</b>, <b>InfluxDb</b>, <b>Redis</b>.</li>
              <li>DevOps : <b>Gitlab-Ci</b>, <b>Docker</b>, <b>Kubernetes</b> (<b>Kustomize</b>, Helm).</li>
              <li>Hébergement : <b>AKS</b> (Azure Kubernetes Service).</li>
              <li>E-Commerce : <b>Vendure</b>.</li>
              <li>Authentification et gestion utilisateurs : <b>Keycloak</b>, token <b>JWT</b>.</li>
              <li>Information Voyageur : <b>Navitia</b>, <b>OpenStretMap</b>, <b>GTFS</b>, NTFS, SIRI.</li>
              <li>Blockchain : <b>Solidity</b> et Token ERC20.</li>
              <li>Géocodeur : Mimirsbrunn et Azure Maps Geocoding.</li>
            </ol><br>`,
            `<b>Tâches Techniques</b>
            <ol>
              <li>Réflexion avec l'architecte sur notre infrastructure</li>
              <li>Accompagnement des nouveaux arrivants (alternants, stagiaires, collaborateurs)</li>
              <li>Formations sur nos outils</li>
              <li>Rédaction des tâches et estimations</li>
              <li>Préparation et revue de sprint</li>
              <li>Code Review</li>
            </ol><br>`,
          ],
        },
        {
          logo: "images/logo/MaaSify.png",
          location: "Clermont-Ferrand (63000) - France",
          subtitle: "Développeur Backend (Distanciel)",
          years: "Septembre 2020 - Juin 2021",
          tasks: [
            `<b>DevOps</b>
            <ol>
              <li>Gestion des mises en production</li>
              <li>Ecriture des déploiements kubernetes avec Helm et Kustomize</li>
              <li>Ecriture de la CI avec Gitlab-CI</li>
              <li>Ecriture des images Docker pour le lancement de nos applications</li>
            </ol><br>`,
            `<b>Gestion des accès utilisateurs</b>
            <ol>
              <li>Mise en place de toute l'authentification et organisation des comptes usagers avec Keycloak</li>
              <li>Administration des rôles et groupes</li>
              <li>Définition de quotas pour chaque tiers</li>
              <li>Gestion des tokens JWT</li>
            </ol><br>`,
            `<b>Monitoring</b>
            <ol>
              <li>Mise en place d'outils de monitoring de notre infrastructure</li>
              <li>Sentry : Remontée de bugs</li>
              <li>Apollo Engine : Usage de nos APIs et suivi de l'évolution</li>
              <li>InfluxDb et Grafana : Remontée en temps réel du fonctionnement de microservices</li>
            </ol><br>`,
            `<b>Création de connecteurs et passerelles</b>
            <ol>
              <li>Avance - retard des transports en commun </li>
              <li>Données dynamiques des points d'intérêts : nombre de places disponibles dans un parking, nombre de vélos disponibles à la location, ...</li>
              <li>Positions temps réels de véhicules (trotinettes, taxis, etc)</li>
            </ol><br>`,
          ],
        },
        {
          logo: "images/logo/MyBus.jpg",
          location: "Clermont-Ferrand (63000) - France",
          subtitle: "Stagiaire Développeur Backend (Hybride)",
          years: "Avril 2020 - Septembre 2020",
          tasks: [
            `<b>Blockchain : Smart Contract avec Ethereum</b>
            <ol>
              <li>Développement d'une blockchain privée</li>
              <li>Création d'un smart contract avec le langage Solidity</li>
              <li>Initialisation du Token MyBus</li>
            </ol><br>`,
            `<b>Extraction de données statiques provenant de différentes sources</b>
            <ol>
              <li>GTFS - Données pour le transport public</li>
              <li>GBFS - Données pour les vélos urbains</li>
              <li>Open Street Map - Données carte du monde</li>
              <li>Remontés de ces informations dans Navitia</li>
            </ol><br>
            `,
            `<b>Missions annexes</b>
            <ol>
              <li>Synchronisation du modèle et de la base de données en utilisant la migration Typeorm </li>
              <li>Dévelopement d'un terminal de paiement virtuel avec la plateforme Dalenys</li>
              <li>Documentation en ligne interactive en utilisant Compodoc et Depencendy Cruiser</li>
              <li>Passage d'une architecture microservices à monorepo</li>
            </ol><br>`,
          ],
        },
        {
          title: "OrcaWise",
          location: "Dublin - Irlande",
          subtitle: "Stagiaire Développeur Full Stack",
          years: "Avril 2019 - Septembre 2019",
          description:
            "Développement d'un nouveau type de CRM. La solution supervise les organisations afin de prédire le prochain acheteur pour un client, conçoit des messages personnalisés et les transmet sur plusieurs canaux pour lancer des conversations. Les prospects sont notés et poussés vers le CRM",
          tasks: [
            `<b>Développement d'une API REST</b>
            <ol>
              <li>.Net Core, C#</li>
              <li>Interaction avec une base de données sur MongoDB (NoSql)</li>
              <li>Sécurisation de l'API dans le but d'être utilisé uniquement par nos clients</li>
              <li>Utilisation de Nginx pour cacher l'URL de l'API</li>
              <li>Token JWT pour établir la connexion entre un utilisateur et l'API</li>
              <li>Chiffrement des données sensibles</li>
              <li>Déploiement à l'intérieur des serveurs de l'entreprise</li>
              <li>Tests Unitaires avec Mockup de la base de données</li>
              <li>Documentation</li>
            </ol><br>`,
            `<b>Développement d'une application web</b>
            <ol>
              <li>Angular 8, HTML, SCSS, TypeScript</li>
              <li>Sécurisation de l'accès au dashboard</li>
              <li>Déploiement à l'intérieur des serveurs de l'entreprise</li>
              <li>Audit sur mon propre site en utilisant WebPageTest et ThinkWithGoogle</li>
            </ol><br>`,
            `<b>NLP: Analyse de phrases provenant de flux RSS</b>
            <ol>
              <li>Python</li>
              <li>Spacy librairie pour appliquer NLP</li>
              <li>Extraction de mots clés provenant de journaux</li>
            </ol><br>`,
            `<b>Trouver des outils open-sources au lieu d'applications payantes</b>
            <ol>
              <li>Windows Server à Linux Server</li>
              <li>Répertoire privé de Github à Gitlab, dans le but d'utiliser Gitlab-Ci</li>
              <li>.NET Framework à .NET Core car gratuit et cross-platform</li>
              <li>Planification des tâches : Jira à Trello pour manager les stagiaires</li>
              <li>Communication : Google Hangout à Slack</li>
            </ol><br>`,
            `<b>Management de 12 stagiaires dans le but de continuer et d'améliorer mes projets</b>
            <ol>
              <li>Organisation de formations</li>
              <li>Ecriture des UserStory pour trois groupes de quatre stagiaires</li>
              <li>Documentation des tâches et des technologies dont ils ont besoin</li>
              <li>Estimation du temps nécessaire</li>
              <li>Donner des conseils et de l'aide s'ils rencontrent des problèmes</li>
            </ol><br>
            `,
          ],
          logo: "images/logo/orcawise.png",
        },
        {
          title: "Lidl",
          location: "Vichy 03200 - France",
          subtitle: "Caissier Employé Libre Service",
          years: "Juillet 2015 - Mars 2019",
          description: "Emploi étudiant : week - ends et vacances scolaires",
          tasks: [
            "Encaissement des clients, dans le <b>respect des procédures</b> imposées par l’entreprise tout en <b>renseignant</b> et <b>conseillant</b> les clients",
            "Mise en rayon de différents types de produits avec aménagement des têtes de gondole",
            "Gestion d’un reliquat, <b>inventaire stock</b>",
            "Diverses <b>formations</b> réalisées autour de mon travail : santé et sécurité au travail, respect de la chaîne du froid, sécurité sanitaire HACCP, tri des déchets, transpalette électrique, auto-laveuse",
            "<b>Travailler en équipe</b> et <b>participer à des réunions</b>",
          ],
          logo: "images/logo/Lidl.png",
        },
      ],
    },
    projects: {
      title: "Projets académiques",
      list: [
        {
          title: "TrackTrip",
          subtitle: "100 heures",
          years: "2020",
          location: "3ème année ISIMA",
          description:
            "Développement d'un réseau social comme Instagram, sur bureau et mobile",
          tasks: [
            "<b>FrontEnd</b> : React (TypeScript) ",
            "<b>BackEnd </b> : API en NodeJs Express",
            "<b>Hébergement FrontEnd</b> : GitLab pages",
            "<b>Hébergement BackEnd</b> : Google Cloud Platform",
            "<b>Intégration Continue</b> : GitLab-CI",
          ],
          logo: "images/icons/tracktrip.png",
        },
        {
          title: "Robot sumo",
          subtitle: "50 heures",
          years: "2017 - 2018",
          location: "1ère année ISIMA",
          description:
            "Projet de robotique où le but est de pousser un autre robot en dehors des limites d'un terrain",
          tasks: [
            "Suivre un <b>cahier des charges</b> précis",
            "Codé en <b>C++</b> et le système d'exploitation <b>Mbed</b>",
            "Participation au <b>tournoi national de robotique</b> (Nîmes - France, Mai 2018)",
          ],
          logo: "images/projects/robot.PNG",
        },
        {
          title: "Voiture radio-télécommandée par smartphone",
          subtitle: "80 heures",
          years: "2014 - 2015",
          location: "Dernière année du Baccalauréat",
          description:
            "Enlever la commande télécommandée de la voiture pour la remplacer par une connexion bluetooth entre le téléphone et le véhicule",
          tasks: [
            "Application mobile avec <b>App Inventor 2</b> (mouvement par bouton ou le gyroscope)",
            "Micro-controlleur codé en <b>Arduino</b>",
            "1er au tournoi régional de l'<b>Olympiade des Sciences de l'Ingénieur</b> (Clermont-Ferrand - France, Avril 2015), suivi de la compétition national en face de professionnels de l'industrie (Paris - France, Mai 2015)",
          ],
          logo: "images/icons/car.png",
        },
      ],
    },
    skills: {
      title: "Compétences",
      list: [
        {
          title: "Langages Objets",
          tasks: [
            "<b>Maîtrisés : </b>NestJs (TypeScript), C# (.NET Core & Framework), C++, Java",
            "<b>Connaissances : </b> Rust, Python, Solidity",
            "<b>Vus : </b> Swift",
          ],
          logo: "images/logo/object.png",
        },
        {
          title: "Base de données",
          tasks: [
            "<b>Maîtrisés : </b>SQL (PostgreSQL), NoSQL (avec MongoDB), InfluxDb, Redis",
            "<b>Connaissances : </b> MySQL",
          ],
          logo: "images/logo/mongodb.png",
        },
        {
          title: "CI / CD",
          tasks: [
            "<b>Maîtrisés : </b>Gitlab CI, Docker, Kubernetes (Helm et Kustomize), AKS",
            "<b>Connaissances : </b> Google Cloud Platform, VMware vSphere",
          ],
          logo: "images/logo/integration.png",
        },
        {
          title: "Documentation et Tests Unitaires",
          tasks: [
            "<b>Documentation : </b>Compodoc, Dependency Cruiser, Doxygen, JavaDoc",
            "<b>Tests Unitaires : </b>Jest (JavaScript), JUnit (Java), WireMock (.NET)",
          ],
          logo: "images/logo/doctest.jpg",
        },
        {
          title: "Outils",
          tasks: [
            "<b>Maîtrisés : </b>RabbitMQ, Grafana, LaTeX, Outils Google, Prezi",
          ],
          logo: "images/logo/tools.png",
        },
        {
          title: "Développement Web",
          tasks: [
            "<b>Maîtrisés : </b>Angular 2+ (TypeScript), React (JavaScript), Material UI, Bootstrap",
            "<b>Connaissances : </b>CSS",
          ],
          logo: "images/logo/web.png",
        },
        {
          title: "Système d'exploitation",
          tasks: [
            "<b>Maîtrisés : </b>Linux, Windows, Android, Mbed",
            "<b>Vus : </b> iOS",
          ],
          logo: "images/logo/os.png",
        },
        {
          title: "Autres langages",
          tasks: [
            "<b>Maîtrisés : </b>Bash, C, Arduino",
            "<b>Connaissances : </b>Script Linux, Assembleur",
            "<b>Vus : </b> Processing, R, Scheme, Caml",
          ],
          logo: "images/logo/others.png",
        },
      ],
    },
    languages: {
      title: "Langues",
      list: [
        {
          title: "Francais",
          subtitle: "Natif",
          logo: "images/flag/fr.png",
        },
        {
          title: "Anglais",
          years: "7 mars 2019",
          location: "63000 Clermont - Ferrand",
          subtitle: "TOEIC : 940/990",
          description:
            "Niveau avant mes 2 stages à l'étranger. Mon anglais est maintenant <b>courant</b>",
          logo: "images/flag/gb.png",
        },
        {
          title: "Espagnol",
          subtitle: "Intermédiaire",
          logo: "images/flag/es.png",
        },
      ],
    },
    hobbies: {
      title: "Centres d'intérêts",
      list: [
        {
          title: "Ceinture Noire de Judo-Jujitsu",
          location: "63122, Ceyrat",
          subtitle: "1ère dan",
          years: "Novembre 2014",
          logo: "images/icons/judo.png",
        },
        {
          title: "Préparation Militaire",
          location: "Haguenau, Camp d'Oberhoffen, France",
          subtitle: "28 ème groupe géographique de l'armée de Terre",
          years: "Juillet 2014",
          logo: "images/logo/army.png",
        },
        {
          title: "Voyage humanitaire",
          location: "Battambang, Cambodge",
          subtitle: "Association SOURIRES",
          years: "Novembre 2015",
          logo: "images/logo/sourires.png",
          description:
            "Construction d'un centre d'accueil pour des enfants orphelins",
        },
        {
          title: "Voyages",
          subtitle:
            "Irlande, Cambodge, Espagne, Portugal, Danemark, Italie, Royaume - Unis, Croatie, Autriche, Allemagne",
          logo: "images/icons/trip.jpg",
        },
        {
          title: "Intérêts",
          subtitle: "Crypto-monnaie, Blockchain, Innovation",
          logo: "images/icons/bitcoin.png",
        },
      ],
    },
  },
  footer: {
    arrow: "Retournez à l'accueil",
    date: "Avril 2022",
  },
};

export default frenchResume;
